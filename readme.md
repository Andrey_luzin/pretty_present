# Pretty Presents

## Install packages
`npm install`
 
## Start
`npm start` - start for development.
`npm run build` - production build.

### Dependencies: 
* node.js (recommended v8.11.4 or later). 