'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');
const prefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const sass = require('gulp-sass');
const cssmin = require('gulp-minify-css');
const imagemin = require('gulp-imagemin');
const pngcrush = require("imagemin-pngcrush");
const browserSync = require("browser-sync");
const clean = require('gulp-clean');
const reload = browserSync.reload;
const sassGlob = require('gulp-sass-glob');

const path = {
  build: {
    html: 'build/',
    js: 'build/js',
    css: 'build/css/',
    img: 'build/imgs/',
    fonts: 'build/fonts/'
  },
  src: {
    html: 'src/html/pages/*.pug',
    js: 'src/js/*.js',
    js_libs: 'src/js/libs/**/*.js',
    style: 'src/css/styles.scss',
    img: 'src/imgs/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  watch: {
    html: 'src/html/**/*.pug',
    js: 'src/js/**/*.js',
    style: 'src/css/**/*.scss',
    img: 'src/imgs/**/*.*',
    fonts: 'src/fonts/**/*.*'
  }
};

const config = {
  server: {
    baseDir: "./build"
  },
  tunnel: true,
  host: 'localhost',
  port: 9000,
  logPrefix: "Pretty"
};

gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('html:build', function(cb) {
  gulp.src(path.src.html)
    .pipe(plumber({
      errorHandler: notify.onError()
    }))
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest(path.build.html))
    .on('end', function() {
      cb();
    })
    .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
  gulp.src([path.src.js_libs, path.src.js])
    .pipe(plumber({
      errorHandler: notify.onError()
    }))
    .pipe(concat('main.js'))
    // .pipe(uglify())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  gulp.src(path.src.style)
    .pipe(plumber({
      errorHandler: notify.onError()
    }))
    .pipe(sassGlob())
    .pipe(sass({
      includePaths: ['src/css/'],
      // outputStyle: 'compressed',
      outputStyle: 'expanded',
      sourceMap: true,
      errLogToConsole: true
    }))
    .pipe(prefixer('last 2 version', 'ie 10'))
    // .pipe(cssmin())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
  gulp.src(path.src.img)
    .pipe(imagemin({
        interlaced: true,
        progressive: true,
        // optimizationLevel: 5,
        svgoPlugins: [
          {
              removeViewBox: false
          }
        ],
        use: [pngcrush()]
    }))
    .pipe(gulp.dest(path.build.img));
    // .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('clean', function () {  
  gulp.src(path.build.html, {read: false})
    .pipe(clean());
});

gulp.task('build', [
  'html:build',
  'js:build',
  'style:build',
  'image:build',
  'fonts:build'
]);

gulp.task('watch', function(){
  watch([path.watch.html], function(event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.img], function(event, cb) {
      gulp.start('image:build');
  });
  watch([path.watch.fonts], function(event, cb) {
    gulp.start('fonts:build');
  });
});

gulp.task('default', ['build', 'webserver', 'watch']);
gulp.task('production', ['clean', 'build']);
