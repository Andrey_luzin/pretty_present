$(document).ready(function(){
  $('.js-reviewSlider').slick({
    customPaging: function(slider, i) {
      $(slider.$slides[i]).data();
      return '<button class="reviewSlider__slideControl">0'+(i+1)+'</button>';
    },
    arrows: false,
    adaptiveHeight: true,
    dots: true,
    dotsClass: 'reviewSlider__navList',
    appendDots: $('.reviewSlider__nav'),
    vertical: true
  });
});
