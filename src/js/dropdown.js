$(document).ready(function (e) {
  function t(t) {
    e(t).bind("click", function (t) {
      t.preventDefault();
      e(this).parent().slideDown(250)
    })
  }
  e(".dropdown__toggler").click(function (event) {
    event.preventDefault();
    var t = e(this).parents(".dropdown").children(".dropdown__menu").is(":hidden");
    e(".dropdown .dropdown__menu").slideUp(250);
    e(".dropdown .dropdown__toggler").removeClass("dropdown__toggler--active");
    if (t) {
      e(this).parents(".dropdown").children(".dropdown__menu").slideToggle(250).parents(".dropdown").children(".dropdown__toggler").addClass("dropdown__toggler--active")
    }
  });
  e(document).bind("click", function (t) {
    var n = e(t.target);
    if (!n.parents().hasClass("dropdown")) e(".dropdown .dropdown__menu").slideUp(250);
  });
  e(document).bind("click", function (t) {
    var n = e(t.target);
    if (!n.parents().hasClass("dropdown")) e(".dropdown .dropdown__toggler").removeClass("dropdown__toggler--active");
  })
});