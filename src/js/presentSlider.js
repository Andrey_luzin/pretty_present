$(document).ready(function(){
  $('.js-presentSlider').slick({
    customPaging: function(slider, i) {
      $(slider.$slides[i]).data();
      return '<button class="presentSlider__slideControl">0'+(i+1)+'</button>';
    },
    arrows: false,
    adaptiveHeight: true,
    dots: true,
    dotsClass: 'presentSlider__navList',
    appendDots: $('.presentSlider__nav'),
    fade: true,
    speed: 800
  });
});
