$(document).ready(function(){
  var mainImg = $('.basketItem__image'),
      preview = $('.basketItem__preview');
  var active = 'basketItem__preview--active';
  preview.click(function() {
    preview.removeClass(active);
    $(this).addClass(active);
    mainImg.attr('src', $(this).find('.basketItem__previewImg').data('src'));
  });
});