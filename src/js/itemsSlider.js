$(document).ready(function(){
  $('.js-itemsSlider').slick({
    appendArrows: $(".js-itemsSliderNav"),
    prevArrow: '<button class="itemsSlider__arrow itemsSlider__arrow--prev"></button>',
    nextArrow: '<button class="itemsSlider__arrow itemsSlider__arrow--next"></button>',
    dots: false,
    slidesToShow: 6,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1590,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      },
      {
        breakpoint: 1480,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  });
});
