$(document).ready(function(){
  $('.js-legoSlider').slick({
    appendArrows: $(".js-legoSliderNav"),
    prevArrow: '<button class="lego__sliderArrow lego__sliderArrow--prev"></button>',
    nextArrow: '<button class="lego__sliderArrow lego__sliderArrow--next"></button>',
    dots: false,
    slidesToShow: 6,
    slidesToScroll: 1,
    adaptiveHeight: true,
    responsive: [
      {
        breakpoint: 1441,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.js-postcard').click(function() {
    $('.js-postcardsList').fadeOut(500, function() {
      $('.js-createPostcard').fadeIn(500);
    });
  })
  $('.js-returnPostcardsList').click(function(e) {
    e.preventDefault();
    $('.js-createPostcard').fadeOut(500, function() {
      $('.js-postcardsList').fadeIn(500);
    });
  })
  
});
