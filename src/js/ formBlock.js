$(document).ready(function(){
  $('#billing_address').change(function() {
    if ($(this).is(":checked")) {
      $('.js-billing').slideDown();
    } else {
      $('.js-billing').slideUp();
    }
  })
});
