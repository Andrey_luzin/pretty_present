$(document).ready(function(){
  var productSlider = $('.js-productSlider');
  productSlider.on('init reInit afterChange', function (event, slick, currentSlide) {
    var i = (currentSlide ? currentSlide : 0) + 1;
    $(".productSlider__countItem").text(i + '/');
    $(".productSlider__countAll").text(slick.slideCount);
  });

  productSlider.slick({
    appendArrows: $(".productSlider__counter"),
    adaptiveHeight: false,
    prevArrow: '<button class="productSlider__arrow productSlider__arrow--prev"></button>',
    nextArrow: '<button class="productSlider__arrow productSlider__arrow--next"></button>',
    dots: false
  });
});
