$(document).ready(function(){
    var picker = new Pikaday(
        {
            field: document.getElementById('datepicker'),
            firstDay: 1,
            minDate: new Date(2000, 0, 1),
            maxDate: new Date(2020, 12, 31),
            yearRange: [2000, 2020],
            bound: false,
            container: document.getElementById('calendar'),
            theme: 'calendar-theme'

        }
    );

    var datepickers = []
    var datepickerInputs = document.querySelectorAll('.calendar__input')
    for (i = 0; i < datepickerInputs.length; i++) {
        var picker = datepickerInputs[i];
        datepickers.push(new Pikaday(
            { 
                field: picker,
                minDate: new Date(),
                theme: 'calendar-theme calendar-theme--input',
                i18n: {
                    previousMonth: 'Предыдущий месяц',
                    nextMonth: 'следующий месяц',
                    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                    weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                    weekdaysShort: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
                    caption: 'Выберите год и месяц'
                }
            }
        ));
    }
});