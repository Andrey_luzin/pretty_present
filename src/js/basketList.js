$(window).on('load', function(){
  resizeBasketHeader()
});

$(window).resize(function(){
  resizeBasketHeader()
});

function resizeBasketHeader() {
  var header = '.basketList__header';
  var footer = '.basketList__footer';
  if ($('.basketItem--simple')[0]) {
    var rowWidth = $(header).parents('.basketList').find('.basketItem--simple')[0].offsetWidth;
    $(header).css({width: rowWidth + 'px'})
    $(footer).css({width: rowWidth + 'px'})
  }
}
