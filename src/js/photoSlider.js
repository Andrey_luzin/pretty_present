$(document).ready(function(){
  $('.js-photoSlider').slick({
    customPaging: function(slider, i) {
      $(slider.$slides[i]).data();
      return '<button class="photoSlider__slideControl">0'+(i+1)+'</button>';
    },
    appendArrows: $(".js-photoSliderNav"),
    adaptiveHeight: true,
    prevArrow: '<button class="photoSlider__arrow photoSlider__arrow--prev"></button>',
    nextArrow: '<button class="photoSlider__arrow photoSlider__arrow--next"></button>',
    dots: true,
    dotsClass: 'photoSlider__nav'
  });
});
