$(document).ready(function(){
  $('.js-companySlider').slick({
    appendArrows: $(".js-companySliderNav"),
    prevArrow: '<button class="companySlider__arrow companySlider__arrow--prev"></button>',
    nextArrow: '<button class="companySlider__arrow companySlider__arrow--next"></button>',
    dots: false,
    slidesToShow: 6,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1441,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }
    ]
  });
});
